package ru.t1.bondarenko.tm;

import ru.t1.bondarenko.tm.constant.ArgumentConst;
import ru.t1.bondarenko.tm.constant.TerminalConst;

import java.util.Scanner;

import static ru.t1.bondarenko.tm.constant.TerminalConst.*;

public class Application {

    public static void main(final String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private static void parseCommands() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.next();
            parseCommand(command);
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private static void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.INFO:
                showDeveloperInfo();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showArgumentError();
        }
        System.exit(0);
    }

    private static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.INFO:
                showDeveloperInfo();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }


    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private static void showDeveloperInfo() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Bondarenko Maksim");
        System.out.println("E-MAIL: iwealth@yandex.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.4.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show developer info.\n", TerminalConst.INFO, ArgumentConst.INFO);
        System.out.printf("%s, %s - Show application version.\n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show application commands.\n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application.\n", EXIT);
    }

}