package ru.t1.bondarenko.tm.constant;

public final class TerminalConst {

    public static final String INFO = "info";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String EXIT = "exit";

}
